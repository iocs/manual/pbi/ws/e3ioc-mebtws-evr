require essioc
require mrfioc2, 2.3.1+15
require sdsioc, 0.0.1+1

#-All systems will have counters for 14Hz, PMorten and DoD
epicsEnvSet("EVREVTARGS" "N0=F14Hz,E0=14,N1=PMortem,E1=40,N2=DoD,E2=42")
#-BI EVRs should have also BPulseSt, BPulseEnd, BiAcqSt Counters
epicsEnvSet("EVREVTARGS" "$(EVREVTARGS),N3=BPulseSt,E3=12,N4=BPulseEnd,E4=13,N5=BiAcqSt,E5=18")

iocshLoad "$(mrfioc2_DIR)/evr.iocsh"      "P=PBI-WS01:Ctrl-EVR-101,PCIID=0e:00.0,EVRDB=$(EVRDB=evr-mtca-300-univ.db)"
dbLoadRecords "evr-databuffer-ess.db"     "P=PBI-WS01:Ctrl-EVR-101"
iocshLoad "$(mrfioc2_DIR)/evrevt.iocsh"   "P=PBI-WS01:Ctrl-EVR-101,$(EVREVTARGS=)"

afterInit('iocshLoad($(mrfioc2_DIR)/evr.r.iocsh                   "P=PBI-WS01:Ctrl-EVR-101, INTREF=#")')
afterInit('iocshLoad($(mrfioc2_DIR)/evrtclk.r.iocsh               "P=PBI-WS01:Ctrl-EVR-101")')

dbLoadRecords "initial-value.template"     "PVNAME=PBI-WS01:Ctrl-EVR-101:DlyGen-1-Delay-SP, VAL=6050"
dbLoadRecords "initial-value.template"     "PVNAME=PBI-WS01:Ctrl-EVR-101:DlyGen-1-Width-SP, VAL=50"
dbLoadRecords "initial-value.template"     "PVNAME=PBI-WS01:Ctrl-EVR-101:DlyGen-1-Evt-Trig0-SP, VAL=18"
dbLoadRecords "initial-value.template"     "PVNAME=PBI-WS01:Ctrl-EVR-101:DlyGen-2-Width-SP, VAL=10"
dbLoadRecords "initial-value.template"     "PVNAME=PBI-WS01:Ctrl-EVR-101:DlyGen-2-Evt-Trig0-SP, VAL=12"
dbLoadRecords "initial-value.template"     "PVNAME=PBI-WS01:Ctrl-EVR-101:DlyGen-3-Width-SP, VAL=10"
dbLoadRecords "initial-value.template"     "PVNAME=PBI-WS01:Ctrl-EVR-101:DlyGen-3-Evt-Trig0-SP, VAL=13"
dbLoadRecords "initial-value.template"     "PVNAME=PBI-WS01:Ctrl-EVR-101:DlyGen-4-Width-SP, VAL=1000"
dbLoadRecords "initial-value.template"     "PVNAME=PBI-WS01:Ctrl-EVR-101:DlyGen-4-Evt-Trig0-SP, VAL=125"
dbLoadRecords "initial-value.template"     "PVNAME=PBI-WS01:Ctrl-EVR-101:DlyGen-5-Width-SP, VAL=1000"
dbLoadRecords "initial-value.template"     "PVNAME=PBI-WS01:Ctrl-EVR-101:DlyGen-5-Evt-Trig0-SP, VAL=40"
dbLoadRecords "initial-value.template"     "PVNAME=PBI-WS01:Ctrl-EVR-101:DlyGen-7-Width-SP, VAL=1000"
dbLoadRecords "initial-value.template"     "PVNAME=PBI-WS01:Ctrl-EVR-101:DlyGen-7-Evt-Trig0-SP, VAL=42"
dbLoadRecords "initial-value.template"     "PVNAME=PBI-WS01:Ctrl-EVR-101:DlyGen-8-Width-SP, VAL=10"
dbLoadRecords "initial-value.template"     "PVNAME=PBI-WS01:Ctrl-EVR-101:DlyGen-8-Evt-Trig0-SP, VAL=10"
dbLoadRecords "initial-value.template"     "PVNAME=PBI-WS01:Ctrl-EVR-101:DlyGen-9-Width-SP, VAL=10"
dbLoadRecords "initial-value.template"     "PVNAME=PBI-WS01:Ctrl-EVR-101:DlyGen-9-Evt-Trig0-SP, VAL=11"
dbLoadRecords "initial-value.template"     "PVNAME=PBI-WS01:Ctrl-EVR-101:DlyGen-10-Width-SP, VAL=10"
dbLoadRecords "initial-value.template"     "PVNAME=PBI-WS01:Ctrl-EVR-101:DlyGen-10-Evt-Trig0-SP, VAL=7"
dbLoadRecords "initial-value.template"     "PVNAME=PBI-WS01:Ctrl-EVR-101:DlyGen-11-Width-SP, VAL=10"
dbLoadRecords "initial-value.template"     "PVNAME=PBI-WS01:Ctrl-EVR-101:DlyGen-11-Evt-Trig0-SP, VAL=6"
dbLoadRecords "initial-value.template"     "PVNAME=PBI-WS01:Ctrl-EVR-101:Out-FP0-Src-SP, VAL=1"
dbLoadRecords "initial-value.template"     "PVNAME=PBI-WS01:Ctrl-EVR-101:Out-FP1-Src-SP, VAL=1"
dbLoadRecords "initial-value.template"     "PVNAME=PBI-WS01:Ctrl-EVR-101:Out-FP2-Src-SP, VAL=1"
dbLoadRecords "initial-value.template"     "PVNAME=PBI-WS01:Ctrl-EVR-101:Out-FP3-Src-SP, VAL=1"
dbLoadRecords "initial-value.template"     "PVNAME=PBI-WS01:Ctrl-EVR-101:Out-FPUV0-Src-SP, VAL=52"
dbLoadRecords "initial-value.template"     "PVNAME=PBI-WS01:Ctrl-EVR-101:Out-FPUV1-Src-SP, VAL=15"
dbLoadRecords "initial-value.template"     "PVNAME=PBI-WS01:Ctrl-EVR-101:Out-FPUV2-Src-SP, VAL=15"
dbLoadRecords "initial-value.template"     "PVNAME=PBI-WS01:Ctrl-EVR-101:Out-FPUV3-Src-SP, VAL=15"







dbLoadRecords "initial-value.template"     "PVNAME=PBI-WS01:Ctrl-EVR-101:DlyGen-0-Delay-SP, VAL=6000"
dbLoadRecords "initial-value.template"     "PVNAME=PBI-WS01:Ctrl-EVR-101:DlyGen-0-Width-SP, VAL=50"
dbLoadRecords "initial-value.template"     "PVNAME=PBI-WS01:Ctrl-EVR-101:DlyGen-0-Evt-Trig0-SP, VAL=18"
dbLoadRecords "initial-value.template"     "PVNAME=PBI-WS01:Ctrl-EVR-101:Out-Back0-Src-SP, VAL=0"
dbLoadRecords "initial-value.template"     "PVNAME=PBI-WS01:Ctrl-EVR-101:Out-Back1-Src-SP, VAL=0"
dbLoadRecords "initial-value.template"     "PVNAME=PBI-WS01:Ctrl-EVR-101:Out-Back2-Src-SP, VAL=0"
dbLoadRecords "initial-value.template"     "PVNAME=PBI-WS01:Ctrl-EVR-101:Out-Back3-Src-SP, VAL=0"
dbLoadRecords "initial-value.template"     "PVNAME=PBI-WS01:Ctrl-EVR-101:Out-Back4-Src-SP, VAL=0"
dbLoadRecords "initial-value.template"     "PVNAME=PBI-WS01:Ctrl-EVR-101:Out-Back5-Src-SP, VAL=0"
dbLoadRecords "initial-value.template"     "PVNAME=PBI-WS01:Ctrl-EVR-101:Out-Back6-Src-SP, VAL=0"
dbLoadRecords "initial-value.template"     "PVNAME=PBI-WS01:Ctrl-EVR-101:Out-Back7-Src-SP, VAL=0"



#- Fixing IdCycle to 64bits
dbLoadRecords "cycleid-64bits.template"               "P=PBI-WS01:Ctrl-EVR-101:"
#- Fixing databuffer Information
dbLoadRecords "BDest.template" "P=PBI-WS01:Ctrl-EVR-101:,PV=BDest-I"
dbLoadRecords "BMod.template" "P=PBI-WS01:Ctrl-EVR-101:,PV=BMod-I"
dbLoadRecords "BPresent.template" "P=PBI-WS01:Ctrl-EVR-101:,PV=BPresent-I"
#- Configuring PVs to be archived
iocshLoad("archive-default.iocsh","P=PBI-WS01:Ctrl-EVR-101:")


#- ----------------------------------------------------------------------------
#- SDS Metadata Capture
#- ----------------------------------------------------------------------------
iocshLoad("$(sdsioc_DIR)/sdsCreateMetadataEVR.iocsh","PEVR=PBI-WS01:Ctrl-EVR-101:,F14Hz=F14Hz")

# Load standard module startup scripts
iocshLoad("$(essioc_DIR)/common_config.iocsh")

